import MenuNav from './MenuNav';
import styles from './Header.module.css';
import linkedin from '../resources/in.gif';
import logo from '../resources/Boucheneb.gif';

export default function Header(props) {
    return <header className= {styles.menu}>
        <div className={styles.gauche}>
            
        <img className={styles.imagelogo} src={logo} alt="logo" />

        </div >

        <div className={styles.gauche}>
                <MenuNav className={styles.menuNav} changePage = {props.changePage} />

                <a href="https://www.linkedin.com/in/allaoua-boucheneb-8b72a71a7?fbclid=IwAR31bHZB5TNQspAmzgymBO1xhRKYkIbe-wMcvP0qNVwfR75YHzJdL-TvaJQ">
                            <img className={styles.image} src={linkedin} alt="Linkedin" />
                            </a>

        </div>
       
    </header>    
   }