/*************************************************************************************************************************************************
 * Pour cette fonction j'ai trouver une video sur Youtube qui permet de changer des images donc je l'ai utiliser dans mon site pour le projet 2  *
 * (Affichages des photos de mon projet) et j'ai changer quelques trucs pour l'adapter a ma page                                                 *
 * le lien de la video est: https://www.youtube.com/watch?v=og3wCO98HkQ                                                                          *
 *************************************************************************************************************************************************/

import React from "react";
import "./Slider.css";
import leftArrow from "../resources/icons/left-arrow.svg";
import rightArrow from "../resources/icons/right-arrow.svg";

export default function BtnSlider({ direction, moveSlide }) {
  console.log(direction, moveSlide);
  return (
    <button
      onClick={moveSlide}
      className={direction === "next" ? "btn-slide next" : "btn-slide prev"}
    >
      <img src={direction === "next" ? rightArrow : leftArrow} alt = "slider"/>
    </button>
  );
}
