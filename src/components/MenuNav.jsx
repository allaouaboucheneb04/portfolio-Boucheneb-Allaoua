import styles from './MenuNav.module.css';
export default function MenuNav(props) {
    return <nav className={styles.grand}>
        <ul className={styles.list}>
            <li className={styles.pages}>
                <a href="/#" onClick={props.changePage('accueil')}>Accueil</a>
            </li>
            <li className={styles.pages}>
                <a href="/#" onClick={props.changePage('projet1')} >Projet 1</a>
            </li>
            <li className={styles.pages}>
                <a href="/#" onClick={props.changePage('projet2')} >Projet 2</a>
            </li>
        </ul>
    </nav>
}