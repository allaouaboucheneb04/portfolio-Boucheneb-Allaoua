import styles from './Projet2.module.css';

import page1Projet2 from '../resources/projet2Capture1.jpg';
import page2Projet2 from '../resources/projet2Capture2.jpg';
import page3Projet2 from '../resources/projet2Capture3.jpg';
import page4Projet2 from '../resources/projet2Capture4.jpg';


export default function Projet2() {
    return <div className={styles.font}>
    
    <main>
        <h2 className={styles.titre}>Test kilman</h2>
        <hr />
        <p className={styles.desc}>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            La méthode Thomas-Kilmann en situation de conflit est la principale mesure des modes de 
            gestion des conflits. L’une des raisons de la popularité est qu’elle montre comment chaque 
            mode de conflit peut être utile pour gérer un conflit dans certaines situations. La méthode Thomas-Kilmann permet aux gens d’apprécier la valeur de leurs propres styles de conflit et également 
            d’apprendre la valeur d’autres styles. L’utilisation de l’évaluation Thomas-Kilmann permet aux gens de se concentrer et de s’appuyer sur leurs forces.
        </p>
        <div className={styles.cat}>
            <h3>Situations</h3>
            <span  className={styles.contenu}>
                <img className={styles.image} src={page1Projet2} alt="Allaoua" />
                <p className={styles.description}>On a une liste de 30 situations chacune des situations 
                contient deux souhaits différent dont les utilisateurs doivent choisir un seul souhait.
                </p>
            </span>

            <h3>Boutton de resultat</h3>

            <span className={styles.contenu}>
                <img className={styles.image} src={page4Projet2} alt="Allaoua" />
                <p className={styles.description}>Aprés que les utilisateurs ont rependu aux 30 situations il ont qu'a cliqué
                sur le boutton "voir mes resultats" pour voir le resultat des réponses choisi.
                </p>
            </span>

            <h3>Diagramme</h3>

            <span className={styles.contenu}>
                <img className={styles.image} src={page2Projet2} alt="Allaoua" />
                <p className={styles.description}>Apres avoir cliqué sur le bouton "voir mes résultats" on voit un diagramme apparaître 
                comme vous le voyez a l'image on calculant les résultat automatiquement.
                </p>
            </span>

            <h3>Resultat</h3>

            <span className={styles.contenu}>
                <img className={styles.image} src={page3Projet2} alt="Allaoua" />
                <p className={styles.description}>
                En dessous du diagramme on trouve le récapitulatif des résultat obtenu en liste
                 et a la fin on a deux bouton le premier est pour sauvegarder les résultat et le deuxième pour imprimer le diagramme obtenu. 
                </p>
            </span>
        </div>
    </main>
    
</div>
}