import profilAllaoua from '../resources/moi.jpg';
import styles from './Accueil.module.css';
import ContentToggler from '../components/ContentToggler';
import cv from '../resources/cvAllaoua.jpg';
import fichierCV from '../resources/cvAllaouaFr.pdf';

export default function Accueil() {
    return <main className={styles.font}>
        <div className={styles.acceuil}>
            <div className={styles.titre1}>
                <div className={styles.nom}>
                    <span className={styles.noms}>
                        Allaoua Boucheneb
                    </span>
                    
                </div>
                <span className={styles.domain}>
                    Créatif
                </span>
                <span className={styles.domaine1}>
                    Front-End Développeur
                </span >

                <ContentToggler title="A propos de moi">
                    <p className={styles.presentation}>
                    Je suis Allaoua Boucheneb, je suis programmeur en 
                    informatique. Ma spécialité est de développer applications
                     bureau et des sites web en Reacts et NodeJS avec les
                      différents langages (C#, Java, Html, CSS, JavaScript).
                    Et ceci est mon portfolio officiel.
                    </p>


                    <div className={styles.contact}>
                        <a href={fichierCV} download={fichierCV}>
                            <img className={styles.imageCV} src={cv} alt="Allaoua" />
                        </a>
                        <p className={styles.email}>Email : allaouaboucheneb04@gmail.com</p>

                    </div>

                </ContentToggler>

            </div>
            <img className={styles.image} src={profilAllaoua} alt="Allaoua" />

        </div>

    </main>
}