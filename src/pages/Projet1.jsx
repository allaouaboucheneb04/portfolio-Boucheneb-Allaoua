import styles from './Projet1.module.css';
import Slider from '../components/Slider';


export default function Projet1() {
    return <div className={styles.font}>
        <main>
                        <h2 className={styles.titre}>Systeme de gestion des stagiaire</h2>
            <hr />
            <p className={styles.desc}>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Ce projet consiste a gérer les stagiaire et les programmes de 
                la cité collégiale, cette application a trois interfaces graphiques 
                dont on trouve dans la première interface qui est la gestion des programmes 
                les fonctionnalité de "l'Ajout", "Modification" ou bien "la suppression " d'un 
                programme dans la base de données que j'ai créé.. </p>
                <p className={styles.desc}>
                Dans la deuxième interface qui est la gestion des stagiaires 
                dont les agents administratifs vont remplir les information personnels 
                des stagiaires avec leurs programme.
                </p>
                <p className={styles.desc}>
                finalement dans la dernière interface on trouve la consultation des 
                données en entrant le nom des programmes et la on tout les stagiaire 
                qui sont inscrit dans le programme sélectionner.
                </p>

                <Slider /> 

        </main>
    </div>
        
}